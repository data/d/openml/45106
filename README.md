# OpenML dataset: MTPL_SHAP_Tutorial

https://www.openml.org/d/45106

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This motor third-part liability (MTPL) pricing dataset describes 1 Mio insurance policies and their corresponding claim counts, see
      Mayer, M., Meier, D. and Wuthrich, M.V. (2023) SHAP for Actuaries: Explain any Model. http://dx.doi.org/10.2139/ssrn.4389797

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45106) of an [OpenML dataset](https://www.openml.org/d/45106). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45106/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45106/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45106/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

